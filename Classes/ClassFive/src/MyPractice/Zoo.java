package MyPractice;

import java.io.Flushable;

public class Zoo {
    public static void main(String[] args) {
        //we can't create instances of abstract class
        //abstract classes are used for inheritance

        Bird birdOne = new Bird(3, "F", 10);
        birdOne.eat();
        birdOne.sleep();

        Fish fishOne = new Fish(10, "M", 10);
        fishOne.swim();
        fishOne.eat();
        fishOne.sleep();

        Chicken chickenOne = new Chicken(1, "M", 7);
        chickenOne.eat();
        chickenOne.sleep();

        Sparrow sparrowOne = new Sparrow(10, "F", 5);
        sparrowOne.fly();
        sparrowOne.eat();
        sparrowOne.sleep();

        Animal sparrowTwo = new Sparrow(1, "M", 4);
        //sparrowTwo.move();

        Animal fishTwo = new Fish(1, "F", 2);
        //fishTwo.move();

        moveAnimal(sparrowTwo);
        moveAnimal(fishTwo);

        Flyable flyableBirdOne = new Sparrow(1, "F", 2);
        flyableBirdOne.fly();
    }

    //polymorphism
    static void moveAnimal(Animal animal) {
        animal.move();
    }
}

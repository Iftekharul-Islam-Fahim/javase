package MyPractice;

public class Fish extends Animal {
    public Fish(int age, String gender, int weightInLbs) {
        super(age, gender, weightInLbs);
    }

    void swim() {
        System.out.println("Swimming...");
    }

    void move() {
        System.out.println("Fish is swimming...");
    }
}

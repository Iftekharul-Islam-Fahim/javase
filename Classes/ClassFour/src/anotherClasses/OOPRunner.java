package anotherClasses;

public class OOPRunner {
    public static void main(String[] args) {
        //humanOne is an instance(object / object variable) of class anotherClasses.Human
        //anotherClasses.Human() is a default(non - parameterized) constructor of class anotherClasses.Human
        Human humanOne = new Human();

        humanOne.name = "Harry Potter";
        humanOne.age = 17;
        humanOne.eyeColor = "green";

        humanOne.speak();

        Human humanTwo = new Human("Percy Jackson", 16, "blue");

        humanTwo.speak();

        Human humanThree = new Human("Misir Ali", 60, "black");

        humanThree.speak();
        /*
            class variables can be accessed by using class, it can be accessed by using objects to update them
            but right way to do that by using class itself
         */
        Human.nationalID = 133014;
        int humanOneNID = Human.nationalID;
        System.out.println(humanOne.name + "'s" + " NID: " + humanOneNID);

        Human.staticMethod();
        System.out.println("PIE = " + Human.PIE);

        // in below for the case only the latest value of the class variable will be printed
        //humanOne.nationalID = 133014;
        //humanTwo.nationalID = 133015;
        //humanThree.nationalID = 133022;
        //System.out.println("humanOne NID: " + humanOne.nationalID);
        //System.out.println("humanTwo NID: " + humanTwo.nationalID);
        //System.out.println("humanThree NID: " + humanThree.nationalID);

        if (humanThree instanceof Human) {
            System.out.println("Yes, HumanThree is an instance of Human class");
        }
    }
}

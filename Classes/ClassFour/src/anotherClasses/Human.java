package anotherClasses;

import parentClass.Earth;

//subclass can not inherit from 'final' base class
public class Human extends Earth {
    /* non - static fields(fields) / member variables(members)
       / instance variables(vary from instance to instance i.e. unique to each object of a class)
    */
    protected String name;
    int age;
    String eyeColor;

    // static fields(fields) / class label variable(class variables)(common for all objects, not dependent on any object)
    /* static modifier: this tells the compiler that there is exactly one copy of this variable in existence,
       regardless of how many times the class has been instantiated
     */
    static int nationalID;

    //constant: static variable and final
    final static float PIE = 3.1416f;

    //object(instance) label block: no need to call; it will automatically executed when object is created
    //act like as constructor but it's run before the constructors
    {
        System.out.println("Run from instance label block");
    }
    //static block: it will executed when program is compiled and load in JVM to run
    //it's run before the instance label block and the constructors
    static {
        System.out.println("Run from static block");
    }

    /* Constructor:
            class must contain a constructor;
            named as like as class name;
            no return type because it returns a class type object;
            called when object is create;
            parameters can be passed;
            there can be more than one constructor in a class
     */
    // default constructor
    public Human() {
        System.out.println("call from default constructor");
    }

    // parameterized constructor(not default)
    Human(String name, int age, String eyeColor) {
        this(); //call the default private constructor
        this.name = name;
        this.age = age;
        this.eyeColor = eyeColor;
        System.out.println("call from parameterized constructor");
    }

    //non - static method
    void speak() {
        System.out.println("Hello, my name is " + name);
        System.out.println("I am " + age + " years old");
        System.out.println("My eye color is " + eyeColor);
        String a = super.species;
        int b = super.testProtected;
    }
    //static method
    static void staticMethod() {
        System.out.println("from static method");
    }

    //can't override final method
//    public void testFinal() {
//
//    }

    private class InnerClass {

    }
}

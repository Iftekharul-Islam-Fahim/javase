package Exercises;

public class AverageOfNumbers {
    public static void main(String[] args) {
        averageOfNumbers(25, 45, 65);
    }
    public static  void averageOfNumbers(double a, double b, double c) {
        double average = (a + b + c) / 3d;
        System.out.println("Average of three numbers is = " + average);
    }
}

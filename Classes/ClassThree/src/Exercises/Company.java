package Exercises;

import java.util.Scanner;

public class Company {
    public static void main(String[] args) {
        Employee employeeOne = new Employee("Employee One", "One", "Mirpur", "01811379154", "14010518536");
        Employee employeeTwo = new Employee("Employee Two", "Two", "Dhanmondi", "01811379155", "14010518537");
        Employee employeeThree = new Employee("Employee Three", "Three", "Jatrabari", "01811379156", "14010518538");
        Employee employeeFour = new Employee("Employee Four", "Three", "Khilkhet", "01811379157", "14010518539");
        Employee employeeFive = new Employee("Employee Five", "Four", "Karwan Bazar", "01811379158", "14010518540");
        Employee employeeSix = new Employee("Employee Six", "Four", "Uttara", "01811379159", "14010518541");
        Employee employeeSeven = new Employee("Employee Seven", "Five", "Science Lab", "01811379160", "14010518542");
        Employee employeeEight = new Employee("Employee Eight", "Five", "Nilkhet", "01811379161", "14010518543");
        Employee employeeNine = new Employee("Employee Nine", "Six", "New Market", "01811379162", "14010518544");
        Employee employeeTen = new Employee("Employee Ten", "Six", "Bashundhara", "01811379163", "14010518545");

        employeeOne.EmployeeDetails();
        employeeTwo.EmployeeDetails();
        employeeThree.EmployeeDetails();
        employeeFour.EmployeeDetails();
        employeeFive.EmployeeDetails();
        employeeSix.EmployeeDetails();
        employeeSeven.EmployeeDetails();
        employeeEight.EmployeeDetails();
        employeeNine.EmployeeDetails();
        employeeTen.EmployeeDetails();

        System.out.print("Basic Salary of a Sixth Grade Employee: ");
        Scanner scanner = new Scanner(System.in);
        float sixthGradeBasic = scanner.nextFloat();
        employeeTen.sixthGradeSalary(sixthGradeBasic);


    }
}

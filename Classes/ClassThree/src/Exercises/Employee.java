package Exercises;

import java.util.Scanner;

public class Employee extends Salary {
    public String employeeName;
    public String employeeGrade;
    public String employeeAddress;
    public String employeeMobile;
    public String employeeAccount;

    public Employee(String employeeName, String employeeGrade, String employeeAddress, String employeeMobile, String employeeAccount) {
        this.employeeName = employeeName;
        this.employeeGrade = employeeGrade;
        this.employeeAddress = employeeAddress;
        this.employeeMobile = employeeMobile;
        this.employeeAccount = employeeAccount;
    }

    public void EmployeeDetails() {
        System.out.println("Name: " + employeeName + " " + "Grade: " + employeeGrade + " " + "Address: " + employeeAddress + " " + "Mobile: " + employeeMobile + " " + "Bank Account: " + employeeAccount);
    }
}

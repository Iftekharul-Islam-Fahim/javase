package Exercises;

public class SmallestNumber {
    public static void main(String[] args) {
        checkSmallestNumber(25, 37, 29);
    }
    public static void checkSmallestNumber(int a, int b, int c) {
        //Solution : 01
//        if (a < b && a < c) {
//            System.out.println(a + " is the smallest number");
//        } else if (b < a && b < c) {
//            System.out.println(b + " is the smallest number");
//        } else {
//            System.out.println(c + " is the smallest number");
//        }

        //Solution : 02
//        if (a < b) {
//            if (a < c) {
//                System.out.println(a + " is the smallest number");
//            } else {
//                System.out.println(c + " is the smallest number");
//            }
//        } else {
//            System.out.println(b + " is the smallest number");
//        }

        //Solution : 03
        System.out.println(Math.min(Math.min(a, b), c) + " is the smallest number");
    }
}

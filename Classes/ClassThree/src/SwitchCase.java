import java.util.Scanner;

public class SwitchCase {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your number: ");
        int number = scanner.nextInt();
        int remainder = number % 2;
        System.out.println("Remainder = " + remainder);

        switch (remainder) {
            case 0:
                System.out.println("The number is even");
                break;
            default:
                System.out.println("The number is odd");
        }
    }
}

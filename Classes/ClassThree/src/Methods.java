public class Methods {
    public static void main(String[] args) {
        boolean isGameOver = true;
        int score = 800;
        int levelCompleted = 5;
        int bonus = 100;

        int highScore = calculateScore(isGameOver, score, levelCompleted, bonus);
        System.out.println("Your final score was = " + highScore);

        score = 10000;
        levelCompleted = 8;
        bonus = 200;

        highScore = calculateScore(isGameOver, score, levelCompleted, bonus);
        System.out.println("Your final score was = " + highScore);

        int highScorePosition = calculateHighScorePosition(1500);
        displayHighScorePosition("Harry Potter", highScorePosition);

        highScorePosition = calculateHighScorePosition(900);
        displayHighScorePosition("James Bond", highScorePosition);

        highScorePosition = calculateHighScorePosition(400);
        displayHighScorePosition("Jason Bourne", highScorePosition);

        highScorePosition = calculateHighScorePosition(50);
        displayHighScorePosition("John Wick", highScorePosition);
    }

    public static int calculateScore(boolean isGameOver, int score, int levelCompleted, int bonus) {
        if (isGameOver) {
            int finalScore = score + (levelCompleted * bonus);
            return finalScore;
        }
        return -1;
    }

    public static void displayHighScorePosition(String playerName, int highScorePosition) {
        System.out.println(playerName + " managed to get into position " + highScorePosition + " on the high score table");
    }

    public static int calculateHighScorePosition(int playerScore) {
        if (playerScore > 1000) {
            return 1;
        } else if (playerScore > 500 && playerScore < 1000) {
            return 2;
        } else if (playerScore > 100 && playerScore < 500) {
            return 3;
        }
        return 4;
    }
}

public class Operators {
    public static void main(String[] args) {
        int result = 1 + 2; // 1 + 2 = 3
        System.out.println("1 + 2 = " + result);
        int previousResult = result; // previousResult = 3
        System.out.println("Previous result = " + previousResult);
        result = previousResult - 1; // 3 - 1 = 2
        System.out.println("3 - 1 = " + result);
        result = result * 10; // 2 * 10 = 20
        System.out.println("2 * 10 = " + result);
        result = result / 5; // 20 / 5 = 4
        System.out.println("20 / 5 = " + result);
        result = result % 3; // the remainder of (4 % 3) = 1
        System.out.println("4 % 3 = " + result);

        //----------Abbreviating Operators--------------------

        //result = result + 1;
        result++; // 1 + 1 = 2
        System.out.println("1 + 1 = " + result);
        //result = result - 1
        result--; // 2 - 1 = 1
        System.out.println("2 - 1 = " + result);
        //result = result + 2
        result += 2; // 1 + 2 = 3
        System.out.println("1 + 2 = " + result);
        //result = result * 10
        result *= 10; // 3 * 10 = 30
        System.out.println("3 * 10 = " + result);
        //result = result / 5
        result /= 5;
        System.out.println("30 / 5 = " + result);
        //result = result - 2
        result -= 2; //6 - 2 = 4
        System.out.println("6 - 2 = " + result);

        boolean isAlienExist = false;
        // (isAlienExist == false) = true
        if (isAlienExist == true);
            System.out.println("No need to scare of Alien!");
            //2ns statement out of default block
            System.out.println("I am not scared of Alien you dummy!");

        //Adding code block or block of code allows us to adding more than one statement
        if (isAlienExist == false) {
            System.out.println("No need to scare of Alien!");
            System.out.println("I am not scared of Alien you dummy!");
        }

        int topScore = 100;
        if (topScore == 100) {
            System.out.println("You got the top score!");
        }
        if (topScore != 100) {
            System.out.println("You got the top score!");
        }
        if (topScore >= 100) {
            System.out.println("You got the top score!");
        }
        if (topScore <= 100) {
            System.out.println("You got the top score!");
        }

        int secondTopScore = 80;
        if (topScore > secondTopScore && secondTopScore < 100) {
            System.out.println("Second top score is smaller than the top score and is less than 100.");
        }
        if (secondTopScore > 90 || secondTopScore <= 90) {
            System.out.println("Either or both of the conditions are true");
        }

        boolean isCar = false;
        if (!isCar) {    //isCar == true or isCar is same; isCar == false or !isCar is same
            System.out.println("condition is true");
        }

        //ternary operator
        //wasCar = isCar ? true : false; means that
        /*
            if (isCar == true) {
                wasCar = true;
            } else {
                wasCar = false;
            }

         */
        boolean wasCar = isCar ? true : false;
        System.out.println("wasCar is = " + wasCar);

        isCar = true;
        wasCar = isCar ? true : false;
        System.out.println("wasCar is = " + wasCar);

        //operators challenge
        double aDoubleValue = 20.00d;
        double anotherDoubleValue = 80.00d;
        double doubleResult = (aDoubleValue + anotherDoubleValue) * 100.00d;
        System.out.println("doubleResult = " + doubleResult);
        double remainderResult = doubleResult % 40.00d;
        System.out.println("remainderResult = " + remainderResult);
        boolean aBooleanValue = remainderResult == 0.00d ? true : false;
        System.out.println("aBooleanValue = " + aBooleanValue);
        if (!aBooleanValue) {
            System.out.println("Got some remainder");
        }
    }
}

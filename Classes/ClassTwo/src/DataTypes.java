public class DataTypes {
    public static void main(String[] args) {
        int myValue = 10000;

        int intMinValue = Integer.MIN_VALUE;
        int intMaxValue = Integer.MAX_VALUE;

        System.out.println("Minimum value for Integer = " + intMinValue);
        System.out.println("Maximum value for Integer = " + intMaxValue);
        System.out.println("Busted maximum value = " + (intMaxValue + 1));
        System.out.println("Busted minimum value = " + (intMinValue - 1));

        //int intMaxTest = 2147483648; //error: integer number too large
        int intMaxTest = 2_147_483_647; //another format to write a number
        System.out.println(intMaxTest);

        byte byteMinValue = Byte.MIN_VALUE;
        byte byteMaxValue = Byte.MAX_VALUE;
        System.out.println("Minimum value for Byte = " + byteMinValue);
        System.out.println("Maximum value for Byte = " + byteMaxValue);

        short shortMinValue = Short.MIN_VALUE;
        short shortMaxValue = Short.MAX_VALUE;
        System.out.println("Minimum value for Short = " + shortMinValue);
        System.out.println("Maximum value for Short = " + shortMaxValue);

        long myLongValue = 2_147_483_648L;

        long longMinValue = Long.MIN_VALUE;
        long longMaxValue = Long.MAX_VALUE;
        System.out.println("Minimum value for Long = " + longMinValue);
        System.out.println("Maximum value for Long = " + longMaxValue);

        int newIntValue = (intMinValue / 2);
        byte newByteValue = (byte) (byteMinValue / 2);
        short newShortValue = (short) (shortMinValue / 2);
        long newLongValue = (longMinValue / 2);
        System.out.println(newLongValue);

        //challenge
        byte aByteValue = 120;
        short aShortValue = 3200;
        int anIntegerValue = 35000;

        long aLongValue = 50000L + 10L * (aByteValue + aShortValue + anIntegerValue);
        System.out.println(aLongValue);
        short shortTotal = (short) ((aByteValue + aShortValue + anIntegerValue) - 6000);
        System.out.println(shortTotal);
        
        //working with real numbers
        float floatMinValue = Float.MIN_VALUE;
        float floatMaxValue = Float.MAX_VALUE;
        System.out.println("Minimum float value = " + floatMinValue);
        System.out.println("Maximum float value = " + floatMaxValue);

        double doubleMinValue = Double.MIN_VALUE;
        double doubleMaxValue = Double.MAX_VALUE;
        System.out.println("Minimum double value = " + doubleMinValue);
        System.out.println("Maximum double value = " + doubleMaxValue);

        int anIntValue = 5;
        float aFloatValue = 5.25f;
        //or
        float anotherFloatValue = (float) 5.25;
        double aDoubleValue = 5.25d;

        int intValue = 5 / 3;
        float floatValue = 5f / 3f;
        double doubleValue = 5d / 3d;

        System.out.println("intValue = " + intValue);
        System.out.println("floatValue = " + floatValue);
        System.out.println("doubleValue = " + doubleValue);

        double pi = 3.14159d;
        double aNumber = 3_37_570.365_24_60_60d;
        System.out.println(pi);
        System.out.println(aNumber);

        //challenge
        double numberOfPounds = 50d;
        double poundsToKilograms = numberOfPounds * .45359237d;
        System.out.println("Pounds to Kilograms = " + poundsToKilograms);

        //Too many characters in character literal
        //char charValue = 'DD';
        char charValue = 'D';
        char unicodeValue = '\u0044';
        System.out.println(charValue);
        System.out.println(unicodeValue);
        char copyrightUnicodeValue = '\u00A9';
        System.out.println(copyrightUnicodeValue);
        char minCharValue = Character.MIN_VALUE;
        char maxCharValue = Character.MAX_VALUE;
        System.out.println("Minimum Character value = " + minCharValue);
        System.out.println("Maximum Character value = " + maxCharValue);

        boolean booleanTrue = true;
        boolean booleanFalse = false;

        boolean isUnderAge = true;
    }
}

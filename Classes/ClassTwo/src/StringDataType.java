public class StringDataType {
    public static void main(String[] args) {
        String aString = "This is a string";
        System.out.println(aString);
        aString = aString + ". This is another one.";
        System.out.println(aString);
        aString = aString + " \u00A9 2020";
        System.out.println(aString);
        String aNumberString = "365.1252";
        aNumberString = aNumberString + "45.54";
        System.out.println(aNumberString);
        int anIntValue = 55;
        aNumberString = aNumberString + anIntValue;
        System.out.println(aNumberString);
        double aDoubleValue = 3470000.570d;
        aNumberString = aNumberString + aDoubleValue;
        System.out.println(aNumberString);
    }
}

package QuestionThree;

public class Earth {
    public static void main(String[] args) {
        //human1, human2 and human3 are instances or object variables
        Human human1 = new Human();
        human1.name = "Tom Hardy";
        human1.age = 28;
        human1.heightInInches = 68;
        human1.eyeColor = "brown";

        human1.speak();
        //NOTE: Shows references to static methods and fields via class instance rather than a class itself.
        //human1.nationalID = 133014; //no need to create object to access the variable, but you can access it to update your variable
        //right way to access  class label variable
        Human.nationalID = 133014;
        System.out.println("Tom's NID: " + Human.nationalID);

        Human human2 = new Human();
        human2.name = "Harry Potter";
        human2.age = 22;
        human2.heightInInches = 66;
        human2.eyeColor = "blue";

        human2.speak();
        Human.nationalID = 133015;
        System.out.println("Harry's NID: " + Human.nationalID);

        Human human3 = new Human("James Bond", 30, 70, "green");
    }
}

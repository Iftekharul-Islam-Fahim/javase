package QuestionThree;
/*
Write few classes with multiple constructor, static and no static blocks. Add member variables and
class level variables. Assign values to those variables using different mechanisms. Print values from the
classes.
 */

//Human is a class i.e. blueprint or format of an object
class Human {
    //fields / member variables / instance variables(vary from instance to instance)
    String name;
    int age;
    int heightInInches;
    String eyeColor;

    //class label variable(common for all objects, not dependent on any object)
    static long nationalID;

    //Constructor: named as like as class name; no return type because it returns a class type object;
    //called when object is create; parameters can be passed;
    //class must contain a constructor
    Human() {
        System.out.println("call from default constructor");
    }

    Human(String name, int age, int heightInInches, String eyeColor) {
        this.name = name;
        this.age = age;
        this.heightInInches = heightInInches;
        this.eyeColor = eyeColor;

        System.out.println("call from parameterized constructor");
    }

    void speak() {
        System.out.println("Hello, my name is " + name);
        System.out.println("I am " + age + " years old");
        System.out.println("I am " + heightInInches + " inches tall");
        System.out.println("My eye color is " + eyeColor);
    }
    void eat() {
        System.out.println("eating...");
    }
    void walk() {
        System.out.println("walking...");
    }
    void work() {
        System.out.println("working...");
    }
}

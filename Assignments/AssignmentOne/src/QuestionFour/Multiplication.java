package QuestionFour;

public class Multiplication extends MathOperator {
    public Multiplication(double first, double second) {
        super(first, second);
    }

    @Override
    public void execute() {
        double result = first * second;

        System.out.println("The result is " + first + " * " + second + " = " + result);
    }

}

package QuestionFour;

import java.util.Scanner;

public class Calculator {
    public void run() {

        Scanner input = new Scanner(System.in);

        System.out.print("Input first number: ");
        double first = input.nextDouble();

        System.out.print("Input Operator (+, -, *, /): ");
        char operator = input.next().charAt(0);

        System.out.print("Input second number: ");
        double second = input.nextDouble();

        MathOperator mathOperator = null;

        switch (operator) {
            case '+':
                mathOperator = new Addition(first, second);
                break;

            case '-':
                mathOperator = new Subtraction(first, second);
                break;

            case '*':
                mathOperator = new Multiplication(first, second);
                break;

            case '/':
                mathOperator = new Division(first, second);
                break;

            default:
                System.out.println("Error! operator is not correct");
                return;
        }
        mathOperator.execute();
    }
}


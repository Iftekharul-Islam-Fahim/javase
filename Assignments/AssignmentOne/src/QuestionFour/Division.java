package QuestionFour;

public class Division extends MathOperator {
    public Division(double first, double second) {
        super(first, second);
    }

    @Override
    public void execute() {
        double result = first / second; // division

        System.out.println("The result is " + first + " / " + second + " = " + result);
    }

}

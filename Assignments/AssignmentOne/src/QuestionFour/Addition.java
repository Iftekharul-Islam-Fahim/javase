package QuestionFour;

public class Addition extends MathOperator {

    public Addition(double first, double second) {
        super(first, second);
    }

    @Override
    public void execute() {
        double result = first + second;

        System.out.println("The result is " + first + " + " + second + " = " + result);
    }
}


package QuestionFour;

abstract class MathOperator {
    protected double first;
    protected double second;

    public MathOperator(double first, double second) {
        this.first = first;
        this.second = second;
    }

    public abstract void execute();
}


package QuestionOne;

import java.util.Scanner;

public class TotalOddEvenCounter {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Starting Number: ");
        int firstInteger = input.nextInt();
        System.out.println("Last Number: ");
        int secondInteger = input.nextInt();
        int totalEvenCount = 0;
        int totalOddCount = 0;

        for (int i = firstInteger; i <= secondInteger; i++) {
            if (i > 0) {
                if (i % 2 == 0) {
                    totalEvenCount += 1;
                } else {
                    totalOddCount += 1;
                }
            }
        }
        System.out.println("Total even number count: " + totalEvenCount);
        System.out.println("Total odd number count: " + totalOddCount);
    }
}

package questions.two;

public class Salary {
    public void sixthGradeSalary(float sixthGradeBasic) {
        double lowestSalary = sixthGradeBasic + (sixthGradeBasic + 0.2) + (sixthGradeBasic + 0.15);
        System.out.println("Total Salary of a Sixth Grade Employee: " + lowestSalary);
    }
}

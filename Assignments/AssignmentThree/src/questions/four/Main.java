package questions.four;

public class Main {
    public static void main(String[] args) {
        Employee employeeOne = new Employee(25000, 100, 150);
        System.out.println(employeeOne.totalSalary());

        employeeOne.setBaseSalary(30000);
        employeeOne.setHourlyRate(150);
        employeeOne.setExtraRate(200);
        System.out.println(employeeOne.totalSalary());
    }
}

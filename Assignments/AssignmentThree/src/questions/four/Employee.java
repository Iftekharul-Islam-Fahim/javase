package questions.four;

public class Employee {
    private float baseSalary;
    private float hourlyRate;
    private float extraRate;

    public Employee(float baseSalary, float hourlyRate, float extraRate) {
        setBaseSalary(baseSalary);
        setHourlyRate(hourlyRate);
        setExtraRate(extraRate);
    }

    public void setBaseSalary(float baseSalary) {
        if (baseSalary > 0) {
            this.baseSalary = baseSalary;
        }
    }

    public void setHourlyRate(float hourlyRate) {
        if (hourlyRate > 0) {
            this.hourlyRate = hourlyRate;
        }
    }

    public void setExtraRate(float extraRate) {
        if (extraRate > 0) {
            this.extraRate = extraRate;
        }
    }

    public float totalSalary() {
        return baseSalary + (hourlyRate * extraRate);
    }
}

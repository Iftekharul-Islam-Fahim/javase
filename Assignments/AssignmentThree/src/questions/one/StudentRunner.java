package questions.one;

public class StudentRunner {
    public static void main(String[] args) {
        Student unknownStudent = new Student();
        Student knownStudent = new Student("John Snow");

        System.out.println(unknownStudent.getStudentName());
        System.out.println(knownStudent.getStudentName());
    }
}

package questions.one;

public class Student {
    private String studentName;

    public Student() {
        this.studentName = "Unknown";
    }

    public Student(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentName() {
        return studentName;
    }
}

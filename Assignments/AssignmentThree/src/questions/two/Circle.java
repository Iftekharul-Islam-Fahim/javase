package questions.two;

public class Circle extends Shape {
    private float radius;
    private final static float PIE = 3.1416f;

    public Circle(float radius) {
        this.radius = radius;
    }

    public float getRadius() {
        return radius;
    }

    @Override
    public float getArea() {
        return PIE * radius * radius;
    }

    @Override
    public float getPerimeter() {
        return 2 * PIE * radius;
    }
}

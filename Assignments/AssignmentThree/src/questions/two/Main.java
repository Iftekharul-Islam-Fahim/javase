package questions.two;

public class Main {
    public static void main(String[] args) {
        Rectangle rectangleOne = new Rectangle(5, 10);
        System.out.println(rectangleOne.getWidth());;
        System.out.println(rectangleOne.getLength());;
        System.out.println(rectangleOne.getArea());;
        System.out.println(rectangleOne.getPerimeter());;

        Circle circleOne = new Circle(5);
        System.out.println(circleOne.getRadius());;
        System.out.println(circleOne.getArea());;
        System.out.println(circleOne.getPerimeter());;
    }
}

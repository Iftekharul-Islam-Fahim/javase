package questions.two;

public class Rectangle extends Shape {
    private float width;
    private float length;

    public Rectangle(float width, float length) {
        this.width = width;
        this.length = length;
    }

    public float getWidth() {
        return width;
    }

    public float getLength() {
        return length;
    }

    @Override
    public float getArea() {
        return width * length;
    }

    @Override
    public float getPerimeter() {
        return 2 * (length + width);
    }
}

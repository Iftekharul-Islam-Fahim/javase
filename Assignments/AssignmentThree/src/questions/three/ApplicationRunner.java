package questions.three;

public class ApplicationRunner {
    public static void main(String[] args) {
        Member member = new Employee("Employee One", 28, "01811379155", "Dhaka", 25000);
        member.printSalary();

        member = new Manager("Manager One", 40, "01791353402", "Dhaka", 40000);
        member.printSalary();
    }
}

package questions.three;

public class Employee extends Member {
    private String specialization;

    public Employee(String name, int age,  String phoneNumber, String address, float salary) {
        super(name, age, phoneNumber, address, salary);
        specialization = "Java Developer";
    }

    public String getSpecialization() {
        return specialization;
    }
}

package questions.three;

public class Member {
    protected String name;
    protected int age;
    protected String phoneNumber;
    protected String address;
    protected float salary;

    public Member(String name, int age, String address, String phoneNumber, float salary) {
        this.name = name;
        this.age = age;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.salary = salary;
    }

    protected void printSalary() {
        System.out.println(salary);
    }
}

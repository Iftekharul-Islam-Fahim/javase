package questions.three;

public class Manager extends Member {
    private String department;

    public Manager(String name, int age, String phoneNumber, String address, float salary) {
        super(name, age, phoneNumber, address, salary);
        department = "HR";
    }

    public String getDepartment() {
        return department;
    }
}
